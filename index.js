const map = [
    "WWWWWWWWWWWWWWWWWWWWW", //x = posicao do array // z = elemento dentro do array
    "W   W     W     W W W",
    "W W W WWW WWWWW W W W",
    "W W W   W     W W   W",
    "W WWWWWWW W WWW W W W",
    "W         W     W W W",
    "W WWW WWWWW WWWWW W W",
    "W W   W   W W     W W",
    "W WWWWW W W W WWW W F",
    "S     W W W W W W WWW",
    "WWWWW W W W W W W W W",
    "W     W W W   W W W W",
    "W WWWWWWW WWWWW W W W",
    "W       W       W   W",
    "WWWWWWWWWWWWWWWWWWWWW",
];

let main = document.getElementById('main') //estou pegando o mapa para colocar as coisas dentro
let airplane = document.createElement('img')
airplane.id = 'airplane'
let count = 0

function labirinto() {
    for (let i = 0; i < map.length; i++) { //conta tudo do map
        let x = document.createElement('div') //cada posicao do mapa (linha)
        x.className = 'linhas'
        main.appendChild(x)
        for (let j = 0; j < map[i].length; j++) { //conta cada elemento da primeira linha ou segunda etc..
            let z = document.createElement('div')
            count++
            x.appendChild(z) //cada letra dentro dessa posicao
            if (map[i][j] === 'W') {
                z.classList.add('parede')
                z.setAttribute('id', count)
            } else if (map[i][j] === 'S') {
                z.classList.add('percurso') //percurso: 1,...2...3..
                z.appendChild(airplane)
                z.setAttribute('id', count)
            } else if (map[i][j] === 'F') {
                z.classList.add('percurso')
                    //percurso: 1,...2...3..
                z.appendChild(airplane)
                z.setAttribute('id', count)
            } else { //else dos espacos em branco
                z.classList.add('percurso')
                z.setAttribute('id', count) //percurso: 1,...2...3..
            }
        }
    }
}
labirinto()

// let percurso = document.querySelector('.percurso')
// let parede = document.querySelector('.parede')

// elementList = document.querySelectorAll(".linhas")
// elementList = document.querySelectorAll(".linhas div")
//22 linhas

// let message = document.createElement('h1')
// let conteudo = document.getElementById('conteudo');
// let message = document.createElement('h1')
// conteudo.appendChild(message)

let conteudo = document.getElementById('main');
let message = document.createElement('h1')
message.className = 'mensagem'
main.appendChild(message)


document.addEventListener("keydown", function(event) {
    keyName = event.key
    let valoresup = document.getElementById((Number(airplane.parentElement.id) - 21).toString())
    let valoresdown = document.getElementById((Number(airplane.parentElement.id) + 21).toString())
    let final = airplane.parentElement.id


    if (keyName === 'ArrowUp' && valoresup.className === 'percurso') {
        valoresup.appendChild(airplane)
        if (final === "188") {
            message.innerText = "Parabéns você venceu a distância!"
        }
    }

    if (keyName === 'ArrowDown' && valoresdown.className === 'percurso') {
        valoresdown.appendChild(airplane)
        if (final === "188") {
            message.innerText = "Parabéns você venceu a distância!"
        }
    }
    if (keyName === 'ArrowRight' && airplane.parentElement.nextSibling.className === 'percurso') {
        airplane.parentElement.nextSibling.appendChild(airplane)
        if (final === "188") {
            message.innerText = "Parabéns você venceu a distância!"
        }
    }
    if (keyName === 'ArrowLeft' && airplane.parentElement.previousSibling.className === 'percurso') {
        airplane.parentElement.previousSibling.appendChild(airplane)
        if (airplane.parentElement.id === "188") {
            message.innerText = "Parabéns você venceu a distância!"
        }
    }
})